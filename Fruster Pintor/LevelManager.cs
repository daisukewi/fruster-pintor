using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna;


namespace Fruster_Pintor
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class LevelManager : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        char[,] tablaSimbolos;
        float cameraPosition;
        float cameraPositionYAxis;

        bool finJuego;
        public Tile[,] tablaTiles;
        public UserControlledSprite jugador1;
        //public UserControlledSprite jugador2;
        public List<Sprite> listaSprites;

        public LevelManager(Game game)
            : base(game)
        {
            tablaTiles = new Tile[Config.anchoTTiles, Config.altoTTiles];
            tablaSimbolos = new char[Config.anchoTTiles, Config.altoTTiles];
        }


        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            StreamReader lector = new StreamReader("Content/Niveles/Nivel.txt");
            String linea = lector.ReadLine();

            //Carga la Tabla de Simbolos desde el fichero
            for (int y = 0; y < Config.altoTTiles; y++)
            {
                for (int x = 0; x < Config.anchoTTiles; x++)
                    tablaSimbolos[x, y] = linea[x];
                linea = lector.ReadLine();
            }

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);

            listaSprites = new List<Sprite>();
            CargarTiles();
            finJuego = false;

            base.LoadContent();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Back))
                LoadContent();

            if (!finJuego)
            {
                foreach (Sprite sprite in listaSprites)
                {
                    sprite.Update(gameTime, Config.tama�oPantalla); 
                }

                if (jugador1 != null)
                    jugador1.Update(gameTime, Config.tama�oPantalla);

                gameLogic();
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// Gestiona el funcionamiento del juego
        /// </summary>
        private void gameLogic()
        {
            foreach (Sprite sprite in listaSprites)
            {
                if (sprite.collisionRect.Intersects(jugador1.collisionRect))
                    finJuego = true;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            ScrollCamera(spriteBatch.GraphicsDevice.Viewport);
            Matrix cameraTransform = Matrix.CreateTranslation(-cameraPosition, -cameraPositionYAxis, 0.0f);
            /* La llamada a este m�todo ya no est� soportada desde la versi�n 4.0
            //spriteBatch.Begin(BlendState.AlphaBlend, SpriteSortMode.Immediate, RasterizerState.CullNone, cameraTransform);*/
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp,
                DepthStencilState.Default, RasterizerState.CullNone, null, cameraTransform);

            //Pinta las Tiles
            for (int x = 0; x < Config.anchoTTiles; x++)
                for (int y = 0; y < Config.altoTTiles; y++)
                    tablaTiles[x, y].Draw(spriteBatch);

            //Pinta los Mosntruos
            foreach (Sprite sprite in listaSprites)
            {
                sprite.Draw(gameTime, spriteBatch);
            }

            //Pinta el Jugador 1
            if (jugador1 != null)
                jugador1.Draw(gameTime, spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void ScrollCamera(Viewport viewport)
        {
            const float ViewMargin = 0.35f;

            // Calculate the edges of the screen.
            float marginWidth = viewport.Width * ViewMargin;
            float marginLeft = cameraPosition + marginWidth;
            //float marginRight = cameraPosition + viewport.Width - marginWidth;
            float marginRight = cameraPosition - marginWidth + Config.anchoPantalla;

            // Calculate the scrolling borders for the Y-axis.  
            const float TopMargin = 0.3f;
            const float BottomMargin = 0.1f;
            float marginTop = cameraPositionYAxis + viewport.Height * TopMargin;

            float marginBottom = cameraPositionYAxis + viewport.Height - viewport.Height * BottomMargin;

            // Calculate how far to scroll when the player is near the edges of the screen.
            float cameraMovement = 0.0f;
            if (jugador1.collisionRect.X < marginLeft)
                cameraMovement = jugador1.collisionRect.X - marginLeft;
            else if (jugador1.collisionRect.X > marginRight)
                cameraMovement = jugador1.collisionRect.X - marginRight;

            // Calculate how far to vertically scroll when the player is near the top or bottom of the screen.  
            float cameraMovementY = 0.0f;
            if (jugador1.collisionRect.Y < marginTop) //above the top margin  
                cameraMovementY = jugador1.collisionRect.Y - marginTop;
            else if (jugador1.collisionRect.Y > marginBottom) //below the bottom margin  
                cameraMovementY = jugador1.collisionRect.Y - marginBottom;

            // Update the camera position, but prevent scrolling off the ends of the level.
            float maxCameraPosition = Config.anchoTile * Config.anchoPantalla - viewport.Width;
            float maxCameraPositionYOffset = Config.altoTile * Config.altoPantalla - viewport.Height;

            cameraPosition = MathHelper.Clamp(cameraPosition + cameraMovement, 0.0f, maxCameraPosition);

            cameraPositionYAxis = MathHelper.Clamp(cameraPositionYAxis + cameraMovementY, 0.0f, maxCameraPositionYOffset);
        }

        private void CargarTiles()
        {
            for (int x = 0; x < Config.anchoTTiles; x++)
                for (int y = 0; y < Config.altoTTiles; y++)
                    tablaTiles[x, y] = NuevaTile(tablaSimbolos[x, y], Config.getAbsolutePosition(x, y));
        }

        private Tile NuevaTile(char simbolo, Vector2 posicion)
        {
            switch (simbolo)
            {
                case '#': return new TileBloque(Game.Content.Load<Texture2D>(@"GFX\tiles\bloque"), posicion);
                case '*': return new TilePlataforma(Game.Content.Load<Texture2D>(@"GFX\tiles\plataforma"), posicion);
                case '.': return new TileVacia(Game.Content.Load<Texture2D>(@"GFX\tiles\nvacia"), posicion);
                case '1': //Jugador
                    cargarJugador(posicion);
                    return new TileVacia(Game.Content.Load<Texture2D>(@"GFX\tiles\nvacia"), posicion);
                case 'A': //Andador
                    cargarAndador(posicion);
                    return new TileVacia(Game.Content.Load<Texture2D>(@"GFX\tiles\nvacia"), posicion);
                case 'B': //Saltador
                    cargarSaltador(posicion);
                    return new TileVacia(Game.Content.Load<Texture2D>(@"GFX\tiles\nvacia"), posicion);
                default: return new TileVacia(Game.Content.Load<Texture2D>(@"GFX\tiles\nvacia"), posicion);
            }
        }

        private void cargarSaltador(Vector2 posicion)
        {
            listaSprites.Add(new MoztruoSaltador(this, Game.Content.Load<Texture2D>(@"GFX\Moztruos\saltador-ph"),
                        posicion, Config.andadorFrameSize, Config.andadorCollisionOffset,
                        Config.andadorStopFrame, Config.andadorSheetSize,
                        Config.PhysicSpeed, Config.PhysicJump, Config.andadorFrameSpeed));
        }

        private void cargarAndador(Vector2 posicion)
        {
            listaSprites.Add(new MoztruoAndador(this, Game.Content.Load<Texture2D>(@"GFX\Moztruos\andador-ph"),
                        posicion, Config.andadorFrameSize, Config.andadorCollisionOffset,
                        Config.andadorStopFrame, Config.andadorSheetSize,
                        Config.PhysicSpeed, Config.PhysicJump, Config.andadorFrameSpeed));
        }

        private void cargarJugador(Vector2 posicion)
        {
            jugador1 = new UserControlledSprite(this, Game.Content.Load<Texture2D>(@"GFX\jugador"),
                        posicion, Config.jugadorFrameSize, Config.jugadorCollisionOffset,
                        Config.jugadorStopFrame, Config.jugadorSheetSize,
                        Config.PhysicSpeed, Config.PhysicJump, Config.jugadorFrameSpeed);
        }

        public bool isGameOver
        {
            get { return finJuego; }
        }

    }
}