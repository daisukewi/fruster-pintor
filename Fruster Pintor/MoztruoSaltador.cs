using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Fruster_Pintor
{
    class MoztruoSaltador : AutomatedSprite
    {
        public MoztruoSaltador(LevelManager level, Texture2D textureImage, Vector2 position, Point frameSize,
            int collisionOffset, int maxSpeed, int jumpForce)
            : base(level, textureImage, position, frameSize, collisionOffset, maxSpeed, jumpForce)
        {
            maxContador = 3000;
        }

        public MoztruoSaltador(LevelManager level, Texture2D textureImage, Vector2 position, Point frameSize,
            int collisionOffset, Point currentFrame, Point sheetSize, int maxSpeed, int jumpForce,
            int millisecondsPerFrame)
            : base(level, textureImage, position, frameSize, collisionOffset, currentFrame,
            sheetSize, maxSpeed, jumpForce, millisecondsPerFrame)
        {
            maxContador = 3000;
        }

        /// <summary>
        /// Es una m�quina de estados que devuelve el estado actual del monstruo
        /// </summary>
        /// <returns>numero del estado actual</returns>
        public override void getState(GameTime gameTime)
        {
            switch (estado)
            {
                // Inicio: empieza quieto
                case INICIO:
                    estado = QUIETO;
                    maxContador = 3000;
                    break;
                // Girando: 
                case VUELTA:
                    // TODO: cosas aqui
                    break;
                // Muerto: 
                case MUERTE: break;
                // Parado: Si lleva mucho tiempo parado se prepara para saltar
                case QUIETO:
                    contador += gameTime.ElapsedGameTime.Milliseconds;
                    if (contador > maxContador)
                    {
                        contador = 0;
                        estado = PREPARACION;
                        maxContador = 500;
                    }
                    break;
                // preparado: cada medio segundo gira y con prob de 30% salta
                case PREPARACION:
                    contador += gameTime.ElapsedGameTime.Milliseconds;
                    if (contador > maxContador)
                    {
                        contador = 0;
                        maxContador = 500;
                        girarHaciaProta();
                        if (Random() < 30)
                        {
                            estado = SALTO;
                        }
                    }
                    break;
                // saltar: si deja de estar en el suelo, esta saltando
                case SALTO:
                    if (!isOnFloor)
                    {
                        estado = SALTANDO;
                    }
                    break;
                // si al saltar toca el suelo, se queda quieto durante 3 seundos
                case SALTANDO:
                    if (isOnFloor)
                    {
                        estado = QUIETO;
                        maxContador = 3000;
                    }
                    break;
            }
        }

        public override void Update(GameTime gameTime, Rectangle clientBounds)
        {
            base.Update(gameTime, clientBounds);
        }

    }
}
