﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Fruster_Pintor
{
    public abstract class Sprite
    {
        const int defaultMillisecondsPerFrame = 16;

        Texture2D textureImage;
        protected Point frameSize;
        int collisionOffset;
        Point currentFrame;
        Point sheetSize;
        int timeSinceLastFrame = 0;
        int millisecondsPerFrame;

        Point startFrame = new Point(0, 0);
        Point jumpFrame = new Point(0, 1);
        Point fallFrame = new Point(0, 2);
        Point shootFrame = new Point(0, 3);

        protected Vector2 lookAt = new Vector2 (1,0);
        protected Vector2 position;
        protected Vector2 speed;

        public Sprite(Texture2D textureImage, Vector2 position, Point frameSize,
            int collisionOffset, Vector2 speed)
            : this(textureImage, position, frameSize, collisionOffset, new Point(0,0),
            new Point(5,1), speed, defaultMillisecondsPerFrame)
        {
        }

        public Sprite(Texture2D textureImage, Vector2 position, Point frameSize,
            int collisionOffset, Point currentFrame, Point sheetSize, Vector2 speed,
            int millisecondsPerFrame)
        {
            this.textureImage = textureImage;
            this.position = position;
            this.frameSize = frameSize;
            this.collisionOffset = collisionOffset;
            this.currentFrame = currentFrame;
            this.sheetSize = sheetSize;
            this.speed = speed;
            this.millisecondsPerFrame = millisecondsPerFrame;
        }

        public virtual void Update(GameTime gameTime, Rectangle clientBounds)
        {
            if (speed.Y == 0)
            {
                timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
                if (timeSinceLastFrame > millisecondsPerFrame)
                {
                    timeSinceLastFrame = 0;
                    if (speed.X == 0)
                        currentFrame = startFrame;
                    else
                    {
                        currentFrame.X++;
                        currentFrame.Y = 0;
                        if (currentFrame.X >= sheetSize.X)
                        {
                            currentFrame.X = 1;
                            /*currentFrame.Y++;
                            if (currentFrame.Y >= sheetSize.Y)
                                currentFrame.Y = 0;*/
                        }
                    }
                }
            }
            else
            {
                if (speed.Y < 0)
                    currentFrame = jumpFrame;
                else
                    currentFrame = fallFrame;
            }
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            SpriteEffects efecto = (lookAt.X < 0) ? SpriteEffects.FlipHorizontally : SpriteEffects.None;

            spriteBatch.Draw(textureImage,
                position,
                new Rectangle(currentFrame.X * frameSize.X,
                    currentFrame.Y * frameSize.Y,
                    frameSize.X, frameSize.Y),
                Color.White, 0, Vector2.Zero,
                1f, efecto, 1f);
        }

        public abstract Vector2 direction
        {
            get;
        }

        public Rectangle collisionRect
        {
            get
            {
                return new Rectangle(
                    (int)position.X + collisionOffset,
                    (int)position.Y + collisionOffset,
                    frameSize.X - (collisionOffset * 2),
                    frameSize.Y - (collisionOffset * 2));
            }
        }

    }
}
