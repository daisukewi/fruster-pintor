using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;


namespace Fruster_Pintor
{
    /// <summary>
    /// This is a game component that implements Tile.
    /// </summary>
    public class TilePlataforma : Tile
    {
        public TilePlataforma(Texture2D textura, Vector2 posicion)
            : base(textura, posicion, Config.spriteRecTilePlataforma, Config.tilePlataformaPasable)
        {
            // TODO: Construct any child components here
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);
        }

        public override int GetLeftDist(int pos)
        {
            return MaxDistance;
        }

        public override int GetRightDist(int pos)
        {
            return -MaxDistance;
        }

        public override int GetTopDist(int pos)
        {
            return (pos <= collisionRect.Top) ? collisionRect.Top - pos : MaxDistance;
        }

        public override int GetBottomDist(int pos)
        {
            return -MaxDistance;
        }
    }
}