using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Fruster_Pintor
{
    class MoztruoAndador : AutomatedSprite
    {
        public MoztruoAndador(LevelManager level, Texture2D textureImage, Vector2 position, Point frameSize,
            int collisionOffset, int maxSpeed, int jumpForce)
            : base(level, textureImage, position, frameSize, collisionOffset, maxSpeed, jumpForce)
        {
            maxContador = 1500;
        }

        public MoztruoAndador(LevelManager level, Texture2D textureImage, Vector2 position, Point frameSize,
            int collisionOffset, Point currentFrame, Point sheetSize, int maxSpeed, int jumpForce,
            int millisecondsPerFrame)
            : base(level, textureImage, position, frameSize, collisionOffset, currentFrame,
            sheetSize, maxSpeed, jumpForce, millisecondsPerFrame)
        {
            maxContador = 1500;
        }

        /// <summary>
        /// Es una m�quina de estados que devuelve el estado actual del monstruo
        /// </summary>
        /// <returns>numero del estado actual</returns>
        public override void getState(GameTime gameTime)
        {
            switch (estado)
            {
                // Inicio: empieza andando
                case INICIO: estado = ANDAR; break;
                // Andando: continua si hay plataformas
                case ANDAR:
                    if (finPlataformas)
                    {
                        estado = QUIETO;
                    }
                    break;
                // Girando: pasa a andar
                case VUELTA: estado = ANDAR; break;
                // Muerto: 
                case MUERTE: break;
                // Parado: Si lleva mucho tiempo parado gira y anda
                case QUIETO:
                    contador += gameTime.ElapsedGameTime.Milliseconds;
                    if (contador > maxContador)
                    {
                        contador = 0;
                        estado = VUELTA;
                    }
                    break;
            }
        }

        public override void Update(GameTime gameTime, Rectangle clientBounds)
        {
            base.Update(gameTime, clientBounds);
        }

    }
}
