using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace Fruster_Pintor
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public abstract class PhysicSprite : Sprite
    {
        protected LevelManager level;
        protected int maxSpeed;
        int jumpForce;
        float aceleration;
        float gravity;
        int maxGravitySpeed;
        bool onFloor = true;
        bool bajando = false;
        bool canJump;
        Boolean choqueLateral = false;


        /// <summary>
        /// Constructor corto para PhysicSprite
        /// </summary>
        /// <param name="level">LevelManager que controla el juego</param>
        /// <param name="textureImage">Textura de sprites para el mu�eco</param>
        /// <param name="position">Posicion del sprite</param>
        /// <param name="frameSize">Tama�o de cada sprite</param>
        /// <param name="collisionOffset">Offset del cuadro interno del tama�o del sprite</param>
        /// <param name="maxSpeed">Velocidad maxima que puede alcanzar</param>
        public PhysicSprite(LevelManager level, Texture2D textureImage, Vector2 position,
            Point frameSize, int collisionOffset, int maxSpeed, int jumpForce)
            : base(textureImage, position, frameSize, collisionOffset, Vector2.Zero)
        {
            this.level = level;
            this.maxSpeed = maxSpeed;
            this.jumpForce = jumpForce;
            this.aceleration = Config.PhysicAceleration;
            this.gravity = Config.PhysicGravity;
            this.maxGravitySpeed = Config.PhysicMaxGravitySpeed;
        }

        public PhysicSprite(LevelManager level, Texture2D textureImage, Vector2 position,
            Point frameSize, int collisionOffset, Point currentFrame, Point sheetSize,
            int maxSpeed, int jumpForce, int millisecondsPerFrame)
            : base(textureImage, position, frameSize, collisionOffset, currentFrame,
            sheetSize, Vector2.Zero, millisecondsPerFrame)
        {
            this.level = level;
            this.maxSpeed = maxSpeed;
            this.jumpForce = jumpForce;
            this.aceleration = Config.PhysicAceleration;
            this.gravity = Config.PhysicGravity;
            this.maxGravitySpeed = Config.PhysicMaxGravitySpeed;
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// <param name="clientBounds">Rectangulo de la pantalla de donde no tiene que salir</param>
        public override void Update(GameTime gameTime, Rectangle clientBounds)
        {
            Move(gameTime);

            // Comprueba que no se salga de la pantalla
            if (collisionRect.Left - maxSpeed < 0)
                position.X = maxSpeed;
            if (collisionRect.Top - jumpForce < 0)
                position.Y = jumpForce;
            if (collisionRect.Right + maxSpeed > clientBounds.Width)
                position.X = clientBounds.Width - frameSize.X - maxSpeed;
            if (collisionRect.Bottom + jumpForce > clientBounds.Height)
            {
                position.Y = clientBounds.Height - frameSize.Y - jumpForce;
                onFloor = true;
                speed.Y = 0;
            }

            base.Update(gameTime, clientBounds);
        }

        /// <summary>
        /// Devuelve si el sprite esta en el suelo o no
        /// </summary>
        public bool isOnFloor
        {
            get { return onFloor; }
        }

        /// <summary>
        /// Calcula la nueva velocidad a la que se mueve el sprite
        /// </summary>
        void CalculateSpeed()
        {
            if (direction.Y < 0 && onFloor && canJump)
            { //SALTO
                speed.Y = -jumpForce;
                onFloor = false;
                canJump = false;
            }
            else if (direction.Y > 0 && onFloor && canJump)
            { //BAJADA
                bajando = true;
            }
            else if (direction.Y == 0 && onFloor)
            {
                //puede volver a saltar
                canJump = true;
            }
            if (!onFloor)
            { //GRAVEDAD
                speed.Y += gravity;
                if (speed.Y > maxGravitySpeed)
                    speed.Y = maxGravitySpeed;
                lookAt.Y = speed.Y;
            }

            if (direction.X != 0)
            { //MOVIMIENTO HORIZONTAL
                if (!onFloor && lookAt.X != direction.X)
                { //Si cambia de direccion en el aire, va mas despacio
                    lookAt.X = direction.X;
                    speed.X = speed.X / 2;
                }
                else if (onFloor)
                {
                    lookAt.X = direction.X;
                    speed.X += direction.X * aceleration;
                    if (Math.Abs(speed.X) > maxSpeed)
                        speed.X = (speed.X > 0) ? maxSpeed : -maxSpeed;
                }
                /* Se puede mover en el aire. Tiene bugs 
                else
                {
                    lookAt.X = direction.X;
                    speed.X += direction.X * aceleration;
                    if (Math.Abs(speed.X) > maxSpeed)
                        speed.X = (speed.X > 0) ? maxSpeed : -maxSpeed;
                }
                */
            }
            else if (onFloor)
            { //DECELERACION HORIZONTAL
                if (speed.X > 0)
                { //derecha
                    speed.X -= aceleration;
                    speed.X = (speed.X <= 0) ? 0 : speed.X;
                }
                else if (speed.X < 0)
                { //izquierda
                    speed.X += aceleration;
                    speed.X = (speed.X >= 0) ? 0 : speed.X;
                }
            }
        }

        /// <summary>
        /// Comprueba las colisiones con las tiles cercanas
        /// y ajusta la velocidad dependiendo de lo cercana que este la tile
        /// </summary>
        void GestionarColision()
        {
            int vertical, horizontal;
            Tile tileVA, tileVB, tileHA, tileHB, tileD;

            //Calcula las Tiles que estan al rededor del mu�eco
            vertical = (speed.X > 0) ? (int)(collisionRect.Right + speed.X) : (int)(collisionRect.Left + speed.X);
            horizontal = (speed.Y > 0) ? (int)(collisionRect.Bottom + speed.Y) : (int)(collisionRect.Top + speed.Y);

            tileHA = level.tablaTiles[Config.getRelPosX(vertical), Config.getRelPosY(position.Y + 1)];
            tileHB = level.tablaTiles[Config.getRelPosX(vertical), Config.getRelPosY(position.Y + frameSize.Y - 1)];
            tileVA = level.tablaTiles[Config.getRelPosX(position.X + 1), Config.getRelPosY(horizontal)];
            tileVB = level.tablaTiles[Config.getRelPosX(position.X + frameSize.X - 1), Config.getRelPosY(horizontal)];
            tileD = level.tablaTiles[Config.getRelPosX(vertical+1), Config.getRelPosY(horizontal)];


            if (speed.X > 0) //Se mueve hacia la Derecha
            {
                speed.X = Math.Min(tileHA.GetLeftDist(collisionRect.Right), speed.X);
                speed.X = Math.Min(tileHB.GetLeftDist(collisionRect.Right), speed.X);
                //speed.X = Math.Min(tileD.GetLeftDist(collisionRect.Right), speed.X);
                if (speed.X == 0) choqueLateral = true; else choqueLateral = false;
            }
            else if (speed.X < 0) //Se mueve hacia la Izquierda
            {
                speed.X = Math.Max(tileHA.GetRightDist(collisionRect.Left), speed.X);
                speed.X = Math.Max(tileHB.GetRightDist(collisionRect.Left), speed.X);
                //speed.X = Math.Max(tileD.GetRightDist(collisionRect.Left), speed.X);
                if (speed.X == 0) choqueLateral = true; else choqueLateral = false;
            }

            if (speed.Y > 0) //Se mueve hacia Abajo
            {
                speed.Y = Math.Min(tileVA.GetTopDist(collisionRect.Bottom), speed.Y);
                speed.Y = Math.Min(tileVB.GetTopDist(collisionRect.Bottom), speed.Y);
                speed.Y = Math.Min(tileD.GetTopDist(collisionRect.Bottom), speed.Y);
                if (speed.Y == 0)
                    onFloor = true;
            }
            else if (speed.Y < 0) //Se mueve hacia Arriba
            {
                speed.Y = Math.Max(tileVA.GetBottomDist(collisionRect.Top), speed.Y);
                speed.Y = Math.Max(tileVB.GetBottomDist(collisionRect.Top), speed.Y);
                speed.Y = Math.Max(tileD.GetBottomDist(collisionRect.Top), speed.Y);
            }

            //Comprueba que siga en el suelo
            if (onFloor)
            {
                try
                {
                    //Coge las tiles que est�n por debajo del mu�eco
                    tileVA = level.tablaTiles[Config.getRelPosX(position.X + 1), Config.getRelPosY(collisionRect.Bottom)];
                    tileVB = level.tablaTiles[Config.getRelPosX(position.X + frameSize.X - 1), Config.getRelPosY(collisionRect.Bottom)];

                    //Sigue en el suelo si se puede apoyar en las tiles de abajo
                    onFloor = tileVA.GetTopDist(collisionRect.Bottom) == 0;
                    onFloor |= tileVB.GetTopDist(collisionRect.Bottom) == 0;
                    if (!onFloor) canJump = false;

                    //Comprueba que tambien puede bajar de una tile si es pasable
                    bajando = bajando && (tileVA.Pasable && tileVB.Pasable);
                    if (bajando)
                    {
                        position.Y += 1;
                        onFloor = false;
                        canJump = false;
                        bajando = false;
                    }
                }
                catch
                {
                    onFloor = true;
                }
            }
        }

        void Move(GameTime gameTime)
        {
            CalculateSpeed();
            GestionarColision();
            position += speed;// *(int)gameTime.ElapsedGameTime.TotalMilliseconds / 10;
        }

        protected void aumentarSpeed()
        {
            //maxSpeed += 1;
        }

        protected void disminuirSpeed()
        {
            //maxSpeed -= 1;
        }

        /// <summary>
        /// Devuelve si ha llegado al final de una serie de plataformas o no
        /// </summary>
        protected bool finPlataformas
        {
            get {
                Tile tileVA, tileVB;

                //Coge las tiles que est�n por debajo de la tile del mu�eco
                tileVA = level.tablaTiles[Config.getRelPosX(position.X - 2), Config.getRelPosY(collisionRect.Bottom + 2)];
                tileVB = level.tablaTiles[Config.getRelPosX(collisionRect.Right + 2), Config.getRelPosY(collisionRect.Bottom + 2)];

                // Si mira a la derecha coge la tile de la derecha
                if (lookAt.X > 0)
                {
                    return (tileVB.GetType() == typeof(TileVacia)) || hayChoqueLateral;
                }
                else // Si mira a la izquierda coge la tile de la izquierda
                {
                    return (tileVA.GetType() == typeof(TileVacia)) || hayChoqueLateral;
                }
            }
        }

        /// <summary>
        /// Devuelve si ha chocado contra un bloque o no
        /// </summary>
        protected bool hayChoqueLateral
        {
            get
            {
                return choqueLateral;
            }
        }

    }
}