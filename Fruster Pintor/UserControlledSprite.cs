﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Fruster_Pintor
{
    public class UserControlledSprite : PhysicSprite
    {
        public UserControlledSprite(LevelManager level, Texture2D textureImage, Vector2 position, Point frameSize,
            int collisionOffset, Point currentFrame, Point sheetSize, int maxSpeed, int jumpForce)
            : base(level, textureImage, position, frameSize, collisionOffset, maxSpeed, jumpForce)
        {
        }

        public UserControlledSprite(LevelManager level, Texture2D textureImage, Vector2 position, Point frameSize,
            int collisionOffset, Point currentFrame, Point sheetSize, int maxSpeed, int jumpForce,
            int millisecondsPerFrame)
            : base(level, textureImage, position, frameSize, collisionOffset, currentFrame,
            sheetSize, maxSpeed, jumpForce, millisecondsPerFrame)
        {
        }

        public override Vector2 direction
        {
            get {
                Vector2 inputDirection = Vector2.Zero;
                KeyboardState keyState = Keyboard.GetState();

                if (keyState.IsKeyDown(Keys.Left))
                    inputDirection.X -= 1;
                if (keyState.IsKeyDown(Keys.Right))
                    inputDirection.X += 1;
                if (keyState.IsKeyDown(Keys.Up))
                    inputDirection.Y -= 1;
                if (keyState.IsKeyDown(Keys.Down))
                    inputDirection.Y += 1;
                if (keyState.IsKeyDown(Keys.A))
                    aumentarSpeed();
                if (keyState.IsKeyDown(Keys.Z))
                    disminuirSpeed();

                GamePadState padState = GamePad.GetState(PlayerIndex.One);
                if (padState.ThumbSticks.Left.X != 0)
                    inputDirection.X += padState.ThumbSticks.Left.X;
                if (padState.ThumbSticks.Left.Y != 0)
                    inputDirection.Y -= padState.ThumbSticks.Left.Y;

                return inputDirection; // *maxSpeed;
            }
        }

        public override void Update(GameTime gameTime, Rectangle clientBounds)
        {

            base.Update(gameTime, clientBounds);
        }

    }
}
