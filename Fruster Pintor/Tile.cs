using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;


namespace Fruster_Pintor
{
    /// <summary>
    /// This is a game component that implements a Tile.
    /// </summary>
    public abstract class Tile
    {
        Vector2 posicion;
        Rectangle spriteRectangle;
        Texture2D grafico;
        bool pasable;
        int maxDistance;

        public Tile(Texture2D textura, Vector2 pos, Rectangle spRectangle, bool pasable)
        {
            posicion = pos;
            spriteRectangle = spRectangle;
            grafico = textura;
            this.pasable = pasable;
            this.maxDistance = Config.PhysicSpeed + 10;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public virtual void Initialize()
        {
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public virtual void Update(GameTime gameTime)
        {
        }

        /// <summary>
        /// Dibuja la tile sea del tipo que sea
        /// </summary>
        /// <param name="sBatch">necesita un SpriteBach para dibujar el grafico.</param>
        /// <param name="absPos">la posicion absoluta de la tile.</param>
        public virtual void Draw(SpriteBatch sBatch)
        {
            Vector2 relPos = new Vector2(spriteRectangle.X, spriteRectangle.Y);

            sBatch.Draw(grafico, posicion + relPos, spriteRectangle, Color.White,
                0, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }

        /// <summary>
        /// Devuelve el rectangulo de colision de la propia tile
        /// </summary>
        public Rectangle collisionRect
        {
            get
            {
                return new Rectangle(
                    (int)posicion.X + spriteRectangle.X,
                    (int)posicion.Y + spriteRectangle.Y,
                    spriteRectangle.Width,
                    spriteRectangle.Height);
            }
        }

        /// <summary>
        /// Devuelve si la tile se puede atravesar por arriba o no
        /// </summary>
        public bool Pasable
        {
            get { return pasable; }
        }

        /// <summary>
        /// Devuelve la distancia maxima a la que puede estar la tile de un sprite
        /// </summary>
        protected int MaxDistance {
            get { return maxDistance; }
        }

        /// <summary>
        /// Devuelve la distancia desde la izquierda de la tile con respecto del sprite
        /// </summary>
        /// <param name="pos">Posicion del sprite</param>
        public abstract int GetLeftDist(int pos);
        /// <summary>
        /// Devuelve la distancia desde la derecha de la tile con respecto del sprite
        /// </summary>
        /// <param name="pos">Posicion del sprite</param>
        public abstract int GetRightDist(int pos);
        /// <summary>
        /// Devuelve la distancia desde arriba de la tile con respecto del sprite
        /// </summary>
        /// <param name="pos">Posicion del sprite</param>
        public abstract int GetTopDist(int pos);
        /// <summary>
        /// Devuelve la distancia desde abajo de la tile con respecto del sprite
        /// </summary>
        /// <param name="pos">Posicion del sprite</param>
        public abstract int GetBottomDist(int pos);
    }
}