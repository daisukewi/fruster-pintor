﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using EasyConfig;

namespace Fruster_Pintor
{
    class Config
    {
        static ConfigFile fileConf = new ConfigFile("Content/Configuracion.ini");


        /* *************************************** *
         * Configuracion de Tiles                  *
         * *************************************** */

        public static int anchoTTiles = fileConf.SettingGroups["TablaTiles"].Settings["Ancho"].GetValueAsInt();

        public static int altoTTiles = fileConf.SettingGroups["TablaTiles"].Settings["Alto"].GetValueAsInt();

        public static int anchoTile = fileConf.SettingGroups["Tiles"].Settings["Ancho"].GetValueAsInt();

        public static int altoTile = fileConf.SettingGroups["Tiles"].Settings["Alto"].GetValueAsInt();

        public static short nTexturasTiles = (short)fileConf.SettingGroups["Tiles"].Settings["NTexturas"].GetValueAsInt();

        public static short indexTexBloque = (short)fileConf.SettingGroups["TileBloque"].Settings["Indice"].GetValueAsInt();

        public static short indexTexPlataforma = (short)fileConf.SettingGroups["TilePlataforma"].Settings["Indice"].GetValueAsInt();

        public static short indexTexVacia = (short)fileConf.SettingGroups["TileVacia"].Settings["Indice"].GetValueAsInt();

        public static Rectangle spriteRecTileBloque
        {
            get
            {
                return new Rectangle(fileConf.SettingGroups["TileBloque"].Settings["X"].GetValueAsInt(),
                    fileConf.SettingGroups["TileBloque"].Settings["Y"].GetValueAsInt(),
                    fileConf.SettingGroups["TileBloque"].Settings["Ancho"].GetValueAsInt(),
                    fileConf.SettingGroups["TileBloque"].Settings["Alto"].GetValueAsInt());
            }
        }

        public static Rectangle spriteRecTilePlataforma
        {
            get
            {
                return new Rectangle(fileConf.SettingGroups["TilePlataforma"].Settings["X"].GetValueAsInt(),
                    fileConf.SettingGroups["TilePlataforma"].Settings["Y"].GetValueAsInt(),
                    fileConf.SettingGroups["TilePlataforma"].Settings["Ancho"].GetValueAsInt(),
                    fileConf.SettingGroups["TilePlataforma"].Settings["Alto"].GetValueAsInt());
            }
        }

        public static Rectangle spriteRecTileVacia
        {
            get
            {
                return new Rectangle(fileConf.SettingGroups["TileVacia"].Settings["X"].GetValueAsInt(),
                    fileConf.SettingGroups["TileVacia"].Settings["Y"].GetValueAsInt(),
                    fileConf.SettingGroups["TileVacia"].Settings["Ancho"].GetValueAsInt(),
                    fileConf.SettingGroups["TileVacia"].Settings["Alto"].GetValueAsInt());
            }
        }

        public static bool tileBloquePasable = fileConf.SettingGroups["TileBloque"].Settings["Pasable"].GetValueAsBool();

        public static bool tilePlataformaPasable = fileConf.SettingGroups["TilePlataforma"].Settings["Pasable"].GetValueAsBool();

        public static bool tileVaciaPasable = fileConf.SettingGroups["TileVacia"].Settings["Pasable"].GetValueAsBool();


        /* *************************************** *
         * Configuracion de Pantalla               *
         * *************************************** */

        public static int anchoPantalla = anchoTTiles * anchoTile;

        public static int altoPantalla = altoTTiles * altoTile;

        public static Point resolucion
        {
            get
            {
                return new Point(fileConf.SettingGroups["Resolucion"].Settings["Ancho"].GetValueAsInt(),
                    fileConf.SettingGroups["Resolucion"].Settings["Alto"].GetValueAsInt());
            }
        }

        public static bool pantallaCompleta
        {
            get
            {
                return fileConf.SettingGroups["Resolucion"].Settings["PantallaCompleta"].GetValueAsBool();
            }
        }

        public static Vector2 offset
        {
            get
            {
                Vector2 punto = new Vector2(0,0);
                /*if (anchoPantalla < resolucion.X)
                    punto.X = (resolucion.X - anchoPantalla) / 2;
                if (altoPantalla < resolucion.Y)
                    punto.Y = (resolucion.Y - altoPantalla) / 2;
                else if (altoPantalla > resolucion.Y)
                    punto.Y = resolucion.Y - altoPantalla;
                */
                return punto;
            }
        }

        public static Rectangle tamañoPantalla
        {
            get
            {
                return new Rectangle((int)offset.X, (int)offset.Y, (int)offset.X + anchoPantalla, (int)offset.Y + altoPantalla);
            }
        }



        /* *************************************** *
         * Configuracion del Jugador               *
         * *************************************** */

        public static Point jugadorFrameSize
        {
            get
            {
                return new Point(fileConf.SettingGroups["Jugador"].Settings["FrameWidth"].GetValueAsInt(),
                    fileConf.SettingGroups["Jugador"].Settings["FrameHeight"].GetValueAsInt());
            }
        }

        public static int jugadorCollisionOffset = fileConf.SettingGroups["Jugador"].Settings["CollisionOffset"].GetValueAsInt();

        public static Point jugadorStopFrame
        {
            get
            {
                return new Point(fileConf.SettingGroups["Jugador"].Settings["StopFrameX"].GetValueAsInt(),
                    fileConf.SettingGroups["Jugador"].Settings["StopFrameY"].GetValueAsInt());
            }
        }

        public static Point jugadorSheetSize
        {
            get
            {
                return new Point(fileConf.SettingGroups["Jugador"].Settings["FrameSheetWidth"].GetValueAsInt(),
                    fileConf.SettingGroups["Jugador"].Settings["FrameSheetHeight"].GetValueAsInt());
            }
        }
        public static int jugadorFrameSpeed = fileConf.SettingGroups["Jugador"].Settings["FrameSpeed"].GetValueAsInt();




        /* *************************************** *
         * Configuracion de los moztruos           *
         * *************************************** */

        public static Point andadorFrameSize
        {
            get
            {
                return new Point(fileConf.SettingGroups["Moztruo"].Settings["FrameWidth"].GetValueAsInt(),
                    fileConf.SettingGroups["Moztruo"].Settings["FrameHeight"].GetValueAsInt());
            }
        }

        public static int andadorCollisionOffset = fileConf.SettingGroups["Moztruo"].Settings["CollisionOffset"].GetValueAsInt();

        public static Point andadorStopFrame
        {
            get
            {
                return new Point(fileConf.SettingGroups["Moztruo"].Settings["StopFrameX"].GetValueAsInt(),
                    fileConf.SettingGroups["Moztruo"].Settings["StopFrameY"].GetValueAsInt());
            }
        }

        public static Point andadorSheetSize
        {
            get
            {
                return new Point(fileConf.SettingGroups["Moztruo"].Settings["FrameSheetWidth"].GetValueAsInt(),
                    fileConf.SettingGroups["Moztruo"].Settings["FrameSheetHeight"].GetValueAsInt());
            }
        }
        public static int andadorFrameSpeed = fileConf.SettingGroups["Moztruo"].Settings["FrameSpeed"].GetValueAsInt();




        /* *************************************** *
         * Configuracion de Fisica                 *
         * *************************************** */

        public static int PhysicSpeed = fileConf.SettingGroups["Physic"].Settings["Speed"].GetValueAsInt();

        public static int PhysicJump = fileConf.SettingGroups["Physic"].Settings["Jump"].GetValueAsInt();

        public static float PhysicAceleration = fileConf.SettingGroups["Physic"].Settings["Aceleration"].GetValueAsFloat();

        public static float PhysicGravity = fileConf.SettingGroups["Physic"].Settings["Gravity"].GetValueAsFloat();

        public static int PhysicMaxGravitySpeed = fileConf.SettingGroups["Physic"].Settings["MaxGravitySpeed"].GetValueAsInt();


        /* *************************************** *
         * Funciones adicionales                   *
         * *************************************** */

        public static Vector2 getAbsolutePosition(int x, int y)
        {
            return offset + new Vector2(x * altoTile, y * altoTile);
        }

        public static Vector2 getRelativePosition(float x, float y)
        {
            return new Vector2((int)(x/anchoTile), (int)(y/altoTile));
        }

        public static int getRelPosX(float x)
        {
            return (int)(x / anchoTile);
        }

        public static int getRelPosY(float y)
        {
            return (int)(y / altoTile);
        }
    }
}
