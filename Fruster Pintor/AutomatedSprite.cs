﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Fruster_Pintor
{
    abstract class AutomatedSprite : PhysicSprite
    {
        public const int INICIO = 0;
        public const int ANDAR = 1;
        public const int VUELTA = 2;
        public const int MUERTE = 3;
        public const int QUIETO = 4;
        public const int PREPARACION = 5;
        public const int SALTO = 6;
        public const int SALTANDO = 7;


        protected int maxContador = 3000;
        protected int contador = 0;

        Vector2 direccion = Vector2.Zero;
        protected int estado = 0;

        public bool estaMuerto = false;

        public AutomatedSprite(LevelManager level, Texture2D textureImage, Vector2 position, Point frameSize,
            int collisionOffset, int maxSpeed, int jumpForce)
            : base(level, textureImage, position, frameSize, collisionOffset, maxSpeed, jumpForce)
        {
            if (Random() < 50)
                lookAt.X = -1;
            else
                lookAt.X = 1;
        }

        public AutomatedSprite(LevelManager level, Texture2D textureImage, Vector2 position, Point frameSize,
            int collisionOffset, Point currentFrame, Point sheetSize, int maxSpeed, int jumpForce,
            int millisecondsPerFrame)
            : base(level, textureImage, position, frameSize, collisionOffset, currentFrame,
            sheetSize, maxSpeed, jumpForce, millisecondsPerFrame)
        {
            if (Random() < 50)
                lookAt.X = -1;
            else
                lookAt.X = 1;
        }

        public override Vector2 direction
        {
            get
            {
                return direccion;
            }
        }

        /// <summary>
        /// Devuelve el estado actual del monstruo
        /// </summary>
        /// <returns>numero del estado actual</returns>
        public abstract void getState(GameTime gameTime);

        /// <summary>
        /// Devuelve un numero aleatorio entre 0 y 100
        /// </summary>
        /// <returns>numero entero entre 0 y 100</returns>
        public int Random()
        {
            Random random = new Random();
            return random.Next(100);
        }

        public override void Update(GameTime gameTime, Rectangle clientBounds)
        {
            getState(gameTime);
            switch (estado)
            {
                case 1: hacerAndar(); break;
                case 2: hacerVuelta(); break;
                case 3: hacerMuerte(); break;
                case 4: hacerQuieto(); break;
                case 5: hacerPreparacion(); break;
                case 6: hacerSalto(); break;
                case 7: saltando(); break;
            }

            base.Update(gameTime, clientBounds);
        }

        private void hacerMuerte()
        {
            estaMuerto = true;
        }

        private void saltando()
        {
            direccion.Y = 0;
            direccion.X = 0;
            if (hayChoqueLateral)
            {
                lookAt.X *= -1;
                speed.X = maxSpeed * lookAt.X;
            }
        }

        private void hacerSalto()
        {
            direccion.Y = -1;
            direccion.X = lookAt.X;
            speed.X = maxSpeed * direccion.X;
        }

        private void hacerPreparacion()
        {
            speed.X = 0;
        }

        protected void girarHaciaProta()
        {
            if (level.jugador1.collisionRect.Right > collisionRect.Right)
            {
                lookAt.X = 1;
            }
            else if (level.jugador1.collisionRect.Left < collisionRect.Left)
            {
                lookAt.X = -1;
            }
        }

        private void hacerQuieto()
        {
            speed.X = 0;
            direccion.X = 0;
        }

        private void hacerVuelta()
        {
            lookAt.X *= -1;
        }

        private void hacerAndar()
        {
            direccion.X = lookAt.X;
            speed.X = maxSpeed * direccion.X;
        }

    }
}
